import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Route, Switch } from "react-router";
import { ForbiddenView } from "./view/ForbiddenView";
import { LoggedView } from "./view/LoggedView";
import { LoginPortal } from "./view/LoginPortal";


export function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={"/"} component={LoginPortal}/>
        <Route path={"/403"} component={ForbiddenView}/>
        <Route path={"/home"} component={LoggedView}/>
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;
