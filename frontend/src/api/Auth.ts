import axios, { AxiosPromise } from "axios";
import { createHash } from "crypto";

export const tokenUserApp = createHash("sha256").update("usergrandchallenges").digest("base64");

export class Auth {
  loadUserInfo(token: string): AxiosPromise<any> {
    return axios.get("/user/current", { params: { tokenUser: token, tokenUserApp: tokenUserApp } });
  }

  login(username: string, password: string) {
    return axios.post("/user/login", `username=${username}&password=${password}&token=${tokenUserApp}`);
  }

  logout(): AxiosPromise<any> {
    window.localStorage && window.localStorage.setItem("logout", Date.now().toString());
    return axios.post("/user/logout");
  }

  updateUserInfo(): AxiosPromise<any> {
    return axios.get("/user/users/update");
  }
}
