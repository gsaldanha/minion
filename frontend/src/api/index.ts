import { User } from "./User";
import { Auth } from './Auth'

export default {
  user: new User(),
  auth: new Auth()
}