import axios, { AxiosPromise } from "axios";
import { createHash } from "crypto";
export const tokenApp = createHash('sha256').update('usergrandchallenges').digest('base64');

export class User{
  load(params: any): AxiosPromise<any> {
    let promise = axios.get('/user', { params });
    promise.then(value => console.log(value.data));
    return promise
  }
}