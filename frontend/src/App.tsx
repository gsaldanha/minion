import React from 'react'
import Routes from './Routes'
import { Router } from 'react-router'
import { history } from 'view/config/history'
import { Provider } from 'react-redux'
import store from './store'

export function App() {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Routes/>
      </Router>
    </Provider>
  )
}
