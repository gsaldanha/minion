import { Alert } from "bold-ui";
import { css, Interpolation, keyframes } from "emotion";
import React from "react";
import { NotificationItem } from "store/modules/notification";

export interface NotificationWrapperProps {
  notifications: NotificationItem[]

  removeNotification(id: NotificationItem["id"]): any

  restartNotificationTimeout(id: NotificationItem["id"])

  stopNotificationTimeout(id: NotificationItem["id"])
}

export function NotificationWrapper(props: NotificationWrapperProps) {
  const { removeNotification, restartNotificationTimeout, stopNotificationTimeout } = props;

  return (
    <div>
      {props.notifications.map(notif => {
        const onClick = () => removeNotification(notif.id);
        const enter = () => stopNotificationTimeout(notif.id);
        const leave = () => restartNotificationTimeout(notif.id);
        return (
          <Alert
            key={notif.id}
            onCloseClick={onClick}
            styles={{
              wrapper: css(notificationStylesClass),
              container: {
                flexGrow: 0,
                flexBasis: 960
              }
            }}
            type={notif.type}
            onMouseEnter={enter}
            onMouseLeave={leave}
          >
            {notif.message}
          </Alert>
        );
      })}
    </div>
  );
}

const createStyles = () => {
  return {
    notificationStyles: {
      borderRight: "none",
      borderLeft: "none",
      borderRadius: "0",
      animation: `${fadeInFromTop} 400ms linear`
    }
  };
};

export const fadeInFromTop = keyframes({
  from: {
    opacity: 0,
    transform: "translateY(-10%)"
  },
  to: {
    opacity: 1,
    transform: "translateY(0)"
  }
});

export const notificationStylesClass: Interpolation = {
  borderRight: "none",
  borderLeft: "none",
  borderRadius: "0",
  animation: `${fadeInFromTop} 400ms linear`
};
export default NotificationWrapper;
