import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { actions } from "store/modules/auth";
import { LoggedView } from "../../view/LoggedView";
import { LoginView } from "../../view/LoginView";

export interface LoginProps {
  onLogin?(): void
}

export default function Login() {
  const dispatch = useDispatch();
  const storageListener = (event: any) => {
    if (event.key === "login") {
      dispatch(actions.checkLogin());
    } else if (event.key === "logout") {
      dispatch(actions.logout());
    }
  };


  useEffect(() => {
    dispatch(actions.checkLogin());
    window.addEventListener("storage", storageListener, false);
  }, []);

  const user = useSelector(state => state["auth"].user);
  console.log(user);

  if (user) {
    return <LoggedView/>;
  } else {
    return <LoginView/>;
  }
}

// class LoginComponent extends React.Component<LoginComponentProps> {
//   componentWillMount() {
//     this.props.checkLogin()
//   }
//
//   componentDidMount() {
//     window.addEventListener('storage', this.storageListener, false)
//   }
//
//   componentWillUnmount() {
//     window.removeEventListener('storage', this.storageListener, false)
//   }
//
//   render() {
//     if (this.props.authState.user) {
//       return this.props.renderHome(this.props.authState)
//     }
//     return <Form onSubmit={this.props.login} onSubmitSucceeded={this.props.onLogin} render={this.renderForm}/>
//   }
//
//   private renderForm = (props: FormRenderProps) =>
//     this.props.renderForm(
//       props,
//       {
//         userFieldName: 'username',
//         passwordFieldName: 'password',
//       },
//       this.props.authState
//     )
//   private storageListener = (event: any) => {
//     if (event.key === 'login') {
//       this.props.checkLogin()
//     } else if (event.key === 'logout') {
//       this.props.logout()
//     }
//   }
// }
//
// const mapStateToProps = (state: any) => ({
//   authState: state.auth,
// })
//
// const mapDispatchToProps = (dispatch: any) => ({
//   checkLogin: (form: any) => dispatch(actions.checkLogin()),
//   login: (form: any) => dispatch(actions.login(form.username, form.password)),
//   logout: () => dispatch(actions.logoutSuccess()),
// })
//
// export interface FormConfig {
//   userFieldName: string
//   passwordFieldName: string
// }


// export const Login = withRouter(
//   connect(
//     mapStateToProps,
//     mapDispatchToProps
//   )(LoginComponent)
// )
