import { StickyContainer } from "bold-ui";
import AxiosErrorInterceptor from "components/AxiosErrorInterceptor";
import NotificationContainer from "components/NotificationContainer";
import React from "react";
import Login from "../components/auth/Login";

export const LoginPortal = () => {
  return (
    <div>
      <StickyContainer>
        <NotificationContainer/>
      </StickyContainer>
      <AxiosErrorInterceptor/>
      <Login/>
    </div>
  );
};

