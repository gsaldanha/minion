import { HFlow, Text, Theme, useStyles, VFlow } from "bold-ui";
import React from "react";
import Image from "images/gc.jpg";
import { FormLogin } from "./login/FormLogin";
import { withRouter } from "react-router";

export function LoginView() {
  const { classes } = useStyles(createStyles);

  return (
    <HFlow hSpacing={0}>
      <HFlow style={classes.leftContainer}>
        <img src={Image} className={classes.imageLogo} alt={"text"}/>
      </HFlow>
      <HFlow style={classes.rightContainer}>
        <VFlow>
          <Text style={classes.welcomeText}> Oi, bem vindo ao projeto Minion </Text>
        </VFlow>
        <HFlow alignItems='center' hSpacing={1.5}>
          <FormLogin/>
        </HFlow>
      </HFlow>
    </HFlow>
  );
}


const createStyles = (theme: Theme) => {
  return {
    leftContainer: {
      background: theme.pallete.surface.main,
      width: "50vw",
      height: "100vh",
      paddingRight: "3.125rem"
    },
    rightContainer: {
      boxShadow: `inset 11px 0px 15px -5px ${theme.pallete.divider}`,
      width: "46vw",
      height: "100vh",
      paddingLeft: "3.5rem",
      background: theme.pallete.surface.main,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "right 3rem bottom 2rem",
      a: {
        textDecoration: "none",
        color: theme.pallete.text.main,
        fontSize: "14px",
        fontWeight: "bold"
      }
    },
    welcomeText: {
      color: theme.pallete.text.secondary,
      fontSize: "24px"
    },
    imageLogo: {
      marginTop: "300px"
    }
  };
};

export default withRouter(LoginView);
