import { useStyles } from "bold-ui";
import React from "react";
import { UserBar } from "./user/UserBar";


export function LoggedView() {
  const { classes } = useStyles(styles);

  return (
    <>
      <div className={classes.container}>
        <header className={classes.header}>
          <UserBar logout={() => {
          }}/>
        </header>
        <nav className={classes.nav}>
          {/*<MenuBarContainer />*/}
        </nav>
      </div>
    </>
  );
}

const styles = () => ({
  container: {
    display: "grid",
    gridTemplateAreas: `"header header"
                                     "nav main"`,
    gridTemplateColumns: "5rem 1fr"
  } as React.CSSProperties,
  header: {
    gridArea: "header"
  } as React.CSSProperties,
  nav: {
    gridArea: "nav"
  } as React.CSSProperties
});