import { HFlow, Text, Theme, Tooltip, useStyles } from "bold-ui";
import React from "react";
import Image from "images/3por4.png";
import GGCLogo from "images/ggclogo.png";
import BMGFLogo from "images/bmgflogo.png";
import { IconButton } from "../../components/icon/IconButton";

export interface UserBarProps {
  userName?: string
  userTeam?: string
  userImgLink?: string

  logout(): any
}

export function UserBar(props: UserBarProps) {
  const { classes } = useStyles(createStyles);


  return (
    <div className={classes.container}>
      <div className={classes.logo}>
        <HFlow>
          <img src={GGCLogo} alt={"logo global grand challenges"}/>
          <img src={BMGFLogo} alt={"logo bill and melinda gates"}/>
        </HFlow>
      </div>

      <HFlow style={classes.user}>
        <Text tag='p'>Gabriel Saldanha</Text>
        <img src={Image} className={classes.photo} alt={"avatar"}/>
      </HFlow>
      <HFlow style={classes.logout}>
        <Tooltip text='Logout'>
          <IconButton size='small' icon='openDoor' onClick={props.logout}/>
        </Tooltip>
      </HFlow>
    </div>
  );
}

const createStyles = (theme: Theme) => ({
  container: {
    borderBottom: `1px solid ${theme.pallete.divider}`,
    display: "flex",
    alignItems: "center",
    padding: "1rem 4rem",
    height: "4rem",
    background: theme.pallete.surface.main,
    color: theme.pallete.text.main
  } as React.CSSProperties,
  logo: {
    flexGrow: 1,
    img: {
      height: "3rem",
      width: "12rem"
    }
  } as React.CSSProperties,
  logoSvg: {
    "#meu, #bridge": {
      fill: `${theme.pallete.text.main} !important`
    }
  } as React.CSSProperties,
  user: {
    borderRight: `1px solid ${theme.pallete.divider}`,
    paddingRight: "1rem",
    height: "6.1rem",
    alignItems: "center"
  } as React.CSSProperties,
  photo: {
    width: "2rem",
    height: "2rem",
    objectFit: "contain",
    borderRadius: "0.25rem"
  } as React.CSSProperties,
  logout: {
    paddingLeft: "0.5rem"
  } as React.CSSProperties,
  lightBulbo: {
    paddingLeft: "0.5rem",
    paddingRight: "0.5rem",
    height: "4rem",
    alignItems: "center"
  } as React.CSSProperties
});
