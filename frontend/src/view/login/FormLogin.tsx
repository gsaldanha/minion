import { Button, Cell, Grid, HFlow, TextField } from "bold-ui";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { actions } from "../../store/modules/auth";
import { createHash } from "crypto";

export function FormLogin() {
  const [formState, setFormState] = useState({
    username: "",
    password: ""
  });

  const dispatch = useDispatch();


  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (formState.username && formState.password) {
      dispatch(
        actions.login(
          formState.username,
          createHash("sha256").update(formState.password).digest("hex")
        )
      );
    }
  };

  const handleChange = (name: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    const el = e.target;

    setFormState(state => ({
      ...state,
      [name]: el.type === "checkbox" ? el.checked : el.value
    }));
  };

  const handleChangePass = (name: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    const el = e.target;

    setFormState(state => {
      return ({
        ...state,
        [name]: el.type === "checkbox" ? el.checked : el.value
      });
    });
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <Grid wrap>
          <Cell xs={6}>
            <TextField
              name='username'
              label='Nome de Usuario ou Email'
              placeholder='Enter your login name'
              value={formState.username}
              onChange={handleChange("username")}
              required
            />
          </Cell>
          <Cell xs={6}>
            <TextField
              name='password'
              label='Senha'
              type='password'
              placeholder='Enter your senha'
              value={formState.password}
              onChange={handleChangePass("password")}
              required={true}
            />
          </Cell>
          <Cell xs={6}>
            <HFlow justifyContent='flex-end'>
              <Button type='submit' kind='primary'>
                Enter
              </Button>
            </HFlow>
          </Cell>
        </Grid>
      </form>
    </>
  );
}
