module.exports.Usuario = class Usuario {
    constructor(id, nome, sobrenome, senha, email, tipo) {
        this.id = id;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.senha = senha;
        this.email = email;
        this.tipo = tipo;
        this.token = null;
    }
};
