
const express = require('express');
const server = require('../Server')
const router = express.Router();
const crypto = require('crypto');
const gc = require("../db/grandchallengeDB");
const {Usuario} = require("../entity/UsuarioDTO");
const tokenNode = crypto.createHash('sha256').update('usergrandchallenges').digest('base64');

/**
 * This function comment is parsed by doctrine
 * @route Get /user/current
 * @group Usuario - Operacoes sobre o Usuario
 * @param {string} tokens.body.required -  - ex: {"username": "NOME OU EMAIL", password: "SENHA EM SHA256 BASE64", "token": "TOKEN DO SERVIDOR"}
 * @returns {object} 200 - Um objeto Json com o token do usuario logado
 * @returns {Error}  Erro Padrao - Usuario nao encontrado
 */
router.get('/current', (req, res, next) => {
    const {tokenUser, tokenUserApp} = req.query;
    const tokenUserAppReplace = tokenUserApp.replace(/ +/gi, '+')
    const tokenUserReplace = tokenUser.replace(/ +/gi, '+')
    if (tokenUserAppReplace === tokenNode) {
        getUserLoggedAndSend(tokenUserReplace, res)
    } else {
        console.log(tokenNode)
        res.sendStatus(401)
    }
});

/**
 * This function comment is parsed by doctrine
 * @route POST /user/login
 * @group Usuario - Operacoes sobre o Usuario
 * @param {string} json.body.required - O servidor vai gravar o Login do usuario e retornar um token para que o client-side possa navegar as requisicoes pelo servidor - eg: {"username": "NOME OU EMAIL", password: "SENHA EM SHA256 BASE64", "token": "TOKEN DO SERVIDOR"}
 * @example {string} {"username": "NOME OU EMAIL", password: "SENHA EM SHA256 BASE64", "token": "TOKEN DO SERVIDOR"}
 * @returns {object} 200 - Um objeto Json com o token do usuario logado
 * @returns {Error}  Erro Padrao - Usuario nao encontrado
 */
router.post('/login', (req, res) => {
    const {username, token, password} = req.body;
    if (token === tokenNode){
        loginUser(username, password, res)
    } else {
        res.sendStatus(401)
    }
})

router.post('/', (req, res) => {
    const {nome, sobrenome, senha, email, tipo, token} = req.body;
    if (token === tokenNode) {
        addUser(new Usuario(null, nome, sobrenome, senha, email, tipo), res);
    } else {
        res.sendStatus(401)
    }
});

router.put('/', (req, res) => {
    const {id, nome, sobrenome, senha, email, tipo, token} = req.body;
    if (token === tokenNode) {
        setUser(new Usuario(id, nome, sobrenome, senha, email, tipo), res)
    } else {
        res.sendStatus(401)
    }
});

async function getUserLoggedAndSend(tokenUser, res) {
    const user = server.Logins.get(tokenUser)
    if(user) {
        res.send(JSON.stringify(user))
    } else (
      res.sendStatus(404)
    )
}

async function loginUser(usuario, senha, res) {
    const consultaUsuario = 'SELECT * FROM minion.usuario WHERE (nome=$1 OR email=$1) AND senha=$2';
    const parametros = [usuario, senha];
    try {
        const resultado = await transacao(consultaUsuario, parametros);
        if (resultado){
            const user = resultado.rows[0]
            const userToken = crypto.createHash('sha256').update(new Date().toTimeString() + user.nome + user.email + tokenNode).digest('base64')

            const userObj = new Usuario(user.id, user.nome, user.sobrenome, user.senha, user.email, user.regra)
            userObj.token = userToken;
            server.Logins.set(userToken, userObj)
            res.send(userToken)
        } else {
            return  res.sendStatus(404);
        }
    } catch (e) {
        console.log(e.stack);
        res.sendStatus(400)
    }
}

async function getUser(usuario, senha) {
    const consultaUsuario = 'SELECT * FROM minion.usuario WHERE (nome=$1 OR email=$1) AND senha=$2';
    const parametros = [usuario, senha];
    try {
        const resultado = await transacao(consultaUsuario, parametros);
        if (resultado) {
            const row = resultado.rows[0];
            return new Usuario(row.id, row.nome, row.sobrenome, row.senha, row.email, row.regra)
        } else {
            return null;
        }
    } catch (e) {
        console.log('[' + new Date().toTimeString() + ']' + e.stack)
        throw e
    }
}

async function addUser(usuario, res) {
    const insereUsuario = 'INSERT INTO minion.usuario ' +
        '(nome, sobrenome, senha, email, regra) VALUES ' +
        '($1, $2, $3, $4, $5)';
    const parametros = [usuario.nome, usuario.sobrenome, usuario.senha, usuario.email, usuario.tipo];
    try {
        const resultado = await transacao(insereUsuario, parametros);
        if (resultado.rowCount === 1 && resultado.command === 'INSERT') {
            res.send(JSON.stringify('Usuario ' + usuario.email + ' inserido com sucesso'))
        } else {
            res.sendStatus(500)
        }
    } catch (e) {
        console.log(e.stack);
        res.sendStatus(400)
    }

}


async function setUser(usuario, res) {
    const atualizaUsuario = 'UPDATE minion.usuario ' +
        'SET nome = $1, sobrenome = $2 , senha = $3, email = $4 ' +
        'WHERE id = $5;';
    const parametros = [usuario.nome, usuario.sobrenome, usuario.senha, usuario.email, usuario.id];
    try {
        const resultado = await transacao(atualizaUsuario, parametros);
        if (resultado.rowCount === 1 && resultado.command === 'UPDATE') {
            res.send(JSON.stringify('Usuario ' + usuario.email + ' atualizado com sucesso'))
        } else {
            res.sendStatus(500)
        }
    } catch (e) {
        console.log(e.stack);
        res.sendStatus(400)
    }
}

async function transacao(query, params) {
    try {
        const client = await gc.connectDB();
        return await client.query(query, params);
    } catch (err) {
        console.log(err.stack)
    }
}

module.exports = router;
