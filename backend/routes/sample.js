const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const gc = require("../db/grandchallengeDB");
const {Sample} = require("../entity/SampleDTO");
const tokenNode = crypto.createHash('sha256').update('samplegrandchallenges').digest('base64');

// TODO Criar Logica para adicionar os tipos de amostras(Origem) em suas tabelas

router.get('/', (req, res) => {
    const {id, token} = req.body;
    if (token === tokenNode) {
        getSample(id, res)
    } else {
        res.sendStatus(401)
    }
});

router.post('/', (req, res) => {
    const {coordenadas, sitio, momento_coleta, data_coleta, origem, token} = req.body;
    if (token === tokenNode) {
        addSample(new Sample(null, coordenadas, sitio, momento_coleta, data_coleta, origem), res);
    } else {
        res.sendStatus(401)
    }
});
router.put('/', (req, res) => {
    const {id, coordenadas, sitio, momento_coleta, data_coleta, origem, token} = req.body;
    if (token === tokenNode) {
        setSample(new Sample(id, coordenadas, sitio, momento_coleta, data_coleta, origem), res)
    } else {
        res.sendStatus(401)
    }
});

async function getSample(id, res) {
    const consultaAmostra = 'SELECT * FROM minion.sample WHERE id=$1';
    const parametros = [id];
    try {
        const resultado = await transacao(consultaAmostra, parametros);
        if (resultado) {
            const row = resultado.rows[0];
            const amostra = new Sample(row.id, row.geom, row.sitio, row.momento_coleta, row.data_coleta, row.origem)
            ;
            res.send(JSON.stringify(amostra))
        } else {
            res.sendStatus(404)
        }
    } catch (e) {
        console.log(e.stack);
        res.sendStatus(400)
    }
}

async function addSample(amostra, res) {
    const insereAmostra = 'INSERT INTO minion.sample ' +
        '(geom, sitio, momento_coleta, data_coleta, origem) VALUES ' +
        '(ST_SetSRID(ST_MakePoint($1, $2), 4326), $3, $4, $5, $6) RETURNING';
    const parametros = [amostra.lati, amostra.long, amostra.sitio, amostra.momento_coleta, amostra.data_coleta, amostra.origem];
    try {
        const resultado = await transacao(insereAmostra, parametros);
        if (resultado.rowCount === 1 && resultado.command === 'INSERT') {
            const sample_id = resultado.rowCount[0].id;
            //do Origin
        } else {
            res.sendStatus(500)
        }
    } catch (e) {
        console.log(e.stack);
        res.sendStatus(400)
    }

}

async function setSample(amostra, res) {
    const atualizaAmostra = 'UPDATE minion.amostra ' +
        'SET ' +
        'WHERE id = $7;';
    const parametros = [amostra.lati, amostra.long, amostra.sitio, amostra.momento_coleta, amostra.data_coleta, amostra.origem, amostra.id];
    try {
        //TODO Update Sample, Origin ...
    } catch (e) {
        console.log(e.stack);
        res.sendStatus(400)
    }
}

async function transacao(query, params) {
    const client = gc.connectDB();
    return await client.query(query, params)
}

module.exports = router;
