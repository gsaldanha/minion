const express = require('express');
const router = express.Router();
const gc = require('../db/grandchallengeDB');
const Usuario = require('../entity/UsuarioDTO');

/* GET users listing. */
router.get('/', function (req, res, next) {
    getUsers(res)
});

async function getUsers(res) {
    const listdeUsuarios = [];
    const consulta = 'SELECT * FROM minion.usuario';
    try {
        const client = gc.connectDB();
        const resultado = await client.query(consulta);
        resultado.rows.map(
            value => listdeUsuarios.push(new Usuario(value.id, value.nome, value.sobrenome, value.senha, value.email, value.regra))
        );
        res.send(JSON.stringify(listdeUsuarios))
    } catch (e) {
        console.log(e.stack);
        res.sendStatus(404)
    }
}

module.exports = router;
