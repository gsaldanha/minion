const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const logger = require("morgan");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const userRouter = require("./routes/user");

// const swaggerUi = require('swagger-ui-express');
const server = express();
// const swaggerDocument = require('./swagger.json');
const expressSwagger = require("express-swagger-generator")(server);
let options = {
  swaggerDefinition: {
    info: {
      description: "Este e um servidor da aplicacao",
      title: "Projeto MiniOn - Global Grand Challenges | UFSC",
      version: "1.0.0"
    },
    host: "localhost:9090",
    produces: [
      "application/json",
      "application/xml"
    ],
    schemes: ["http"]
    // securityDefinitions: {
    //     JWT: {
    //         type: 'apiKey',
    //         in: 'header',
    //         name: 'Authorization',
    //         description: "",
    //     }
    // }
  },
  basedir: __dirname, //app absolute path
  files: ["./routes/**/*.js"] //Path to the API handle folder
};
expressSwagger(options);

module.exports.Logins = new Map();

// server.use('/api-docs', swaggerUi.serve);
// server.get('/api-docs', swaggerUi.setup(swaggerDocument));

server.use(logger("dev"));
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
server.use(cookieParser());
server.use(express.static(path.join(__dirname, "public")));

server.use("/", indexRouter);
server.use("/users", usersRouter);
server.use("/user", userRouter);

server.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.setHeader("Access-Control-Allow-Headers", "content-type");
  res.setHeader("Content-Type", "application/json");
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

const port = 9090;
server.listen(port, function() {
  console.log("Servidor Web rodando na porta: " + port);
});

showIp();


function showIp() {
  const os = require("os");
  const ifaces = os.networkInterfaces();

  Object.keys(ifaces).forEach(function(ifname) {
    let alias = 0;

    ifaces[ifname].forEach(function(iface) {
      if ("IPv4" !== iface.family || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return;
      }

      if (alias >= 1) {
        // this single interface has multiple ipv4 addresses
        console.log(ifname + ":" + alias, iface.address);
      } else {
        // this interface has only one ipv4 adress
        console.log(ifname, iface.address);

      }
      console.log("Endereco do Servidor:", "http://" + iface.address + ":" + port);
      console.log("Documentacao da API(Swagger):", "http://" + iface.address + ":" + port + "/api-docs");
      ++alias;
    });
  });
}